package com.wgu;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Challenge challenge = new Challenge();

        System.out.print("Da Mighty Code Challenge");
        System.out.print("(Enter 'X' to exit)");

        String input;
        while (true) {
            System.out.print("\nInput: ");
            input = in.next();
            if (input.equalsIgnoreCase("x")) {
                break;
            }

            System.out.printf("Results: %s\n", challenge.processString(input));
        }
    }
}
