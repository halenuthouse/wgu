package com.wgu;

import java.util.HashSet;

/**
 * This Problem Solving Exercise modifies a string using the following rules:
 * 1. Each word in the input string is replaced with the following: the first letter of the word, the count of distinct letters between the first and last letter, and the last letter of the word. For example, "Automotive" would be replaced by "A6e".
 * 2. A "word" is defined as a sequence of alphabetic characters, delimited by any non-alphabetic characters.
 * 3. Any non-alphabetic character in the input string should appear in the output string in its original relative location.
 */
class Challenge {
    /**
     * Inputs a string and processes it per business rules.
     * @param input String to be modified
     * @return String modified per problem solving exercise business rules
     */
    String processString(String input) {
        StringBuilder ret = new StringBuilder();
        StringBuilder word = new StringBuilder();

        // Loop through each character in the input string
        for (Character ch : input.toCharArray()) {
            if (Character.isLetter(ch)) {
                word.append(ch);
            } else {
                // If we have a word, then process it
                if (word.length() > 0) {
                    ret.append(processWord(word));
                    word = new StringBuilder();
                }
                ret.append(ch);
            }
        }

        // Ensure that
        if (word.length() > 0) {
            ret.append(processWord(word));
        }

        return ret.toString();
    }

    /**
     * Replaces the string containing a word
     * @param word String containing the word
     * @return String containing the processed word
     */
    private String processWord(StringBuilder word) {
        HashSet<Character> unique = new HashSet<>();

        for (int i = 1; i < word.length() - 1; i++) {
            unique.add(word.charAt(i));
        }

        return String.format("%c%d%c", word.charAt(0), unique.size(), word.charAt(word.length() - 1));
    }
}
