package com.wgu;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Perform unit tests on the {@link Challenge} class.
 */
public class Tests {
    private class test {
        String input;
        String expected;

        test(String input, String expected) {
            this.input = input;
            this.expected = expected;
        }
    }

    @Test
    public void tests() {
        test[] tests = {
                new test("Automotive", "A6e"),
                new test("1defabc1 cb!a %%%def1123", "1d4c1 c0b!a0a %%%d1f1123"),
                new test("a hot character", "a0a h1t c6r"),
                new test("at", "a0t"),
                new test("x", "x0x"),
                new test("tipu-is-smart", "t2u-i0s-s3t"),
                new test("1234567890", "1234567890"),
                new test("a98779079745092740", "a0a98779079745092740"),
                new test("98779079745092740a", "98779079745092740a0a")
        };

        Challenge challenge = new Challenge();
        for (test t : tests) {
            String results = challenge.processString(t.input);
            assertEquals(t.expected, results);
        }
    }
}
